from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
from wolframclient.deserializers import WXFConsumer, binary_deserialize

import numpy as np

class ComplexFunctionConsumer(WXFConsumer):
    """Implement a consumer that maps Complex to python complex types."""
    def build_function(self, head, args, **kwargs):
        # represent the symbol Complex as a Python class
        Complex = wl.Complex
        if head == Complex and len(args) == 2:
            return complex(*args)
        else:
            return super().build_function(head, args, **kwargs)


def convert_to_python_complex(mma_complex, session):
    """
    mma_complex: Complex[-0.12774090372243024, 0.000381720208311797]
    session: an instance of WolframLanguageSession
    returns
    python complex number
    """
    wxf = session.evaluate_wxf(mma_complex)
    return binary_deserialize(wxf, consumer=ComplexFunctionConsumer())


def gen_eccimr_expr(q=1, x0=0.07, e0=0.1, l0=0, phi0=0, t0=0, t1=0, t2=10000, dt=1):
    """
    generate the string that the Wolfram Language expression function
    `wlexpr` expects in order to generate an EccIMR waveform from
    `EccentricIMRWavefrom`

    it should look like this

    'EccentricIMRWaveform<|"q" -> 1, "x0" -> 0.07, "e0" -> 0.1, "l0" -> 0, "phi0" -> 0, "t0" -> 0|>, {0, 10000, 1}]'

    """
    wl_string = f'EccentricIMRWaveform[<|"q" -> {q}, "x0" -> {x0}, "e0" -> {e0}, "l0" -> {l0}, "phi0" -> {phi0}, "t0" -> {t0}|>, {{{t1}, {t2}, {dt}}}]'
    return wl_string


def generate_eccimr_waveform(session, q=1, x0=0.07, e0=0.1, l0=0, phi0=0, t0=0, t1=0, t2=10000, dt=1):
    """
    session: mathematica session
    """

    wl_string = gen_eccimr_expr(q=q, x0=x0, e0=e0, l0=l0, phi0=phi0, t0=t0, t1=t1, t2=t2, dt=dt)
    mma_hEcc = session.evaluate( wlexpr(wl_string) )

    times = []
    h = []

    for t, mma_c in mma_hEcc:
        times.append(t)
        h.append(convert_to_python_complex(mma_c, session))

    times = np.array(times)
    h = np.array(h)

    return times, h


"""

make sure you have loaded the mma module

module load mathematica/12.0.0

"""

from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wl, wlexpr
from wolframclient.deserializers import WXFConsumer, binary_deserialize

# represent the symbol Complex as a Python class
Complex = wl.Complex

class ComplexFunctionConsumer(WXFConsumer):
    """Implement a consumer that maps Complex to python complex types."""
    def build_function(self, head, args, **kwargs):
        if head == Complex and len(args) == 2:
            return complex(*args)
        else:
            return super().build_function(head, args, **kwargs)


def convert_to_python_complex(mma_complex, session):
    """
    mma_complex: Complex[-0.12774090372243024, 0.000381720208311797]
    session: an instance of WolframLanguageSession
    returns
    python complex number
    """
    wxf = session.evaluate_wxf(mma_complex)
    return binary_deserialize(wxf, consumer=ComplexFunctionConsumer())


kernel="/apps/local/languages/mathematica/12.0.0/bin/WolframKernel"

times = []
h = []

with WolframLanguageSession(kernel=kernel) as session:
    session.evaluate( wlexpr('<< EccentricIMR`;') )
    mma_hEcc = session.evaluate( wlexpr('EccentricIMRWaveform[<|"q" -> 1, "x0" -> 0.07, "e0" -> 0.1, "l0" -> 0, "phi0" -> 0, "t0" -> 0|>, {0, 10000}]') )
    # this should return a 2-`tuple` where the length is the number of time samples
    # the first of the tuple is the time
    # the second of the tuple is a complex number in Mathematica language
    # so we have to convert this to a python complex data type.

    for t, mma_c in mma_hEcc:
        times.append(t)
        h.append(convert_to_python_complex(mma_c, session))


#print(t)
#print(h)

import numpy as np


t = np.array(times)
h = np.array(h)

print(t.shape)
print(h.shape)

import matplotlib.pyplot as plt

plt.figure()
plt.plot(t, np.real(h))
plt.savefig("/home/sebastian.khan/public_html/LVC/example_ecc.png")








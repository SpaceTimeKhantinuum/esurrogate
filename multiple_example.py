import utils
import numpy as np

from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wlexpr

kernel="/apps/local/languages/mathematica/12.0.0/bin/WolframKernel"

e0s = np.arange(0., 0.2, 0.05)

all_times = []
all_h = []


with WolframLanguageSession(kernel=kernel) as session:
    session.evaluate( wlexpr('<< EccentricIMR`;') )
    for e0 in e0s:
        times, h = utils.generate_eccimr_waveform(session, q=1, x0=0.07, e0=e0, l0=0, phi0=0, t0=0, t1=0, t2=10000, dt=1)
        all_times.append(times)
        all_h.append(h)





import matplotlib.pyplot as plt

plt.figure()
for i in range(len(all_times)):
    plt.plot(all_times[i], np.real(all_h[i]), label=f"e = {e0s[i]}")
plt.legend()
plt.savefig("/home/sebastian.khan/public_html/LVC/esurrogate/multi_example_ecc.png")

plt.figure()
for i in range(len(all_times)):
    plt.plot(all_times[i], np.real(all_h[i]), label=f"e = {e0s[i]}")
plt.legend()
plt.xlim(2400, 3000)
plt.savefig("/home/sebastian.khan/public_html/LVC/esurrogate/multi_example_ecc_zoom.png")

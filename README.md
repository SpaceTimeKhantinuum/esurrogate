# esurrogate

Repo looking at eccentric surrogate model.

This surrogate is based on [Hinder+2017](https://arxiv.org/abs/1709.02007)
and the github repo [EccentricIMR](https://github.com/ianhinder/EccentricIMR)

## Development

```
conda create -n esurrogate-dev python=3.7
```

## Mathematica

Attempting to use [Wolfram Client For Python](https://reference.wolfram.com/language/WolframClientForPython/)
to call the eccentricIMR mathematica model in python

### MMA Troubleshooting

```
$ ipython
>>> from wolframclient.evaluation import WolframLanguageSession
>>> from wolframclient.language import wl, wlexpr
>>> session = WolframLanguageSession()
WolframKernelException: Cannot locate a kernel automatically. Please provide an explicit kernel path.
```

answer found [here](https://github.com/WolframResearch/WolframClientForPython/issues/13)

```
$ math
In[1]:= First@$CommandLine
(*use the output as the `kernel` argument*)
/apps/local/languages/mathematica/12.0.0/SystemFiles/Kernel/Binaries/Linux-x86-64/WolframKernel
```

```
$ ipython
>>> kernel=OUTPUT_FROM_ABOVE
>>> from wolframclient.evaluation import WolframLanguageSession
>>> from wolframclient.language import wl, wlexpr
>>> session = WolframLanguageSession(kernel=kernel)
```

If that doesn't work then [this](https://mathematica.stackexchange.com/questions/199958/how-to-get-python-wolframclient-library-to-connect-with-wolframkernel-on-linux)
says to try the following

```
$ which WolframKernel
/apps/local/languages/mathematica/12.0.0/bin/WolframKernel
```

and use this as the `kernel`

import utils
import numpy as np

from wolframclient.evaluation import WolframLanguageSession
from wolframclient.language import wlexpr

kernel="/apps/local/languages/mathematica/12.0.0/bin/WolframKernel"

with WolframLanguageSession(kernel=kernel) as session:
    session.evaluate( wlexpr('<< EccentricIMR`;') )
    times, h = utils.generate_eccimr_waveform(session, q=1, x0=0.07, e0=0.1, l0=0, phi0=0, t0=0, t1=0, t2=10000, dt=1)

import matplotlib.pyplot as plt

plt.figure()
plt.plot(times, np.real(h))
plt.savefig("/home/sebastian.khan/public_html/LVC/esurrogate/util_example_ecc.png")
